from django import forms
from accounts.models import *
from inbox.models import *
from inbox.choices import *

class MessageForm(forms.Form):
    msg_content = forms.CharField(label="Write a reply", max_length=500, widget=forms.Textarea)

class ConversationForm(forms.Form):
    category = forms.ChoiceField(choices = CONVERSATION_CATEGORY, label='Topic', required=True)
    msg_content = forms.CharField(label="Write a message", max_length=500, widget=forms.Textarea)



# class ConversationForm2(forms.Form):
#     # recipient = forms.CharField(max_length=100)
#     msg_content = forms.CharField(label="Write a message", max_length=500, widget=forms.Textarea)
#     # class Meta():
#     #     model = Conversation
#     #     fields = '__all__'

#     def __init__(self, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         self.user = kwargs.pop('user',None)
#         super(ConversationForm2, self).__init__(*args, **kwargs)
#         if self.user.user_type == 4:
#             organization = self.user.organization_profile
#             landlords = User.objects.filter(landlord_profile__organization=organization)
#             tenants = User.objects.filter(tenant_profile__organization=organization)
#             merchants = User.objects.filter(merchant_profile__organizations=organization)
#             users = landlords | tenants | merchants  
#             self.fields['recipient'] = forms.ModelChoiceField(queryset=users)
#         # else:




