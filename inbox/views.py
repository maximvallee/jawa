from django.shortcuts import render, redirect
from django.contrib.auth.decorators import user_passes_test, login_required
from jawa.decorators import post_signup_required, conversation_required
from inbox.models import *
from django.db.models import Q
from inbox.forms import *
from django.db import transaction
from datetime import date
from datetime import datetime as dt
from django.utils import timezone
from itertools import chain

# Create your views here.

@login_required
def list_conversations(request):
    user = request.user
    thread= Conversation.objects.filter(Q(message__receiver=user) | Q(message__sender=user)).distinct().order_by('-updated')
    conversation_list = []

    # import pdb; pdb.set_trace()
    all_con_status_active_user = ConversationStatus.objects.filter(user=request.user).order_by('read')  


    # import pdb; pdb.set_trace()
    for conversation in thread:
        ### Check if any unread message for this conversation & this user
        all_status_list = MessageStatus.objects.filter(message__in=conversation.message.all())
        unread_list = all_status_list.filter(user=request.user, is_read=False)
        ### If no unread messages for this conversation & user, update conversation to read
        if not unread_list.exists():
            obj = conversation.conversation_status.all().get(user=request.user)
            obj.is_read = True
            obj.read=dt.now(timezone.utc)
            obj.save()

    context = {
        'thread' : all_con_status_active_user
    }
    return render(request, 'inbox/conversation_list.html', context)

@login_required
@conversation_required
def read_or_send_message(request,pk):
    # import pdb; pdb.set_trace()
    
    conversation = Conversation.objects.get(id=pk)
    sender = request.user
    receiver = conversation.recipient if conversation.creator == sender else conversation.creator
    message_list = conversation.message.all().order_by('-created')

    all_status_list = MessageStatus.objects.filter(message__in=conversation.message.all()) ## List of all Message Statuses associated to this conversation
    recipient_status_list = all_status_list.filter(user=request.user, is_read=False) ## Filter to current user & that is_read == False
    recipient_status_list.update(is_read=True, read=dt.now(timezone.utc))

    # import pdb; pdb.set_trace()
    conv_status_list = ConversationStatus.objects.filter(conversation=pk) ## List of all Conversation Statuses associated to this conversation
    user_conv_status_list = conv_status_list.filter(user=request.user, is_read=False)
    user_conv_status_list.update(is_read=True, read=dt.now(timezone.utc))

    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            message = Message.objects.create(
                conversation=conversation,
                sender=sender,
                receiver=receiver,
                msg_content=data['msg_content'],
            )
            message.save()

            return redirect('inbox:message_read_or_send', pk=pk )
    else:
        form = MessageForm()
    # import pdb; pdb.set_trace()
    context = {
        'form': form,
        'message_list' : message_list,
        'conversation': conversation
    }
    return render(request, 'inbox/message_form.html', context)

@login_required
@transaction.atomic
def create_conversation(request, username):
    sender = request.user
    # import pdb; pdb.set_trace()
    recipient = User.objects.get(username=username)

    if request.method == "POST":
        form = ConversationForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            conversation = Conversation.objects.create(
                creator=sender,
                recipient=recipient,
                category=data['category']
            )
            conversation.save()
            message = Message.objects.create(
                conversation=conversation,
                sender=sender,
                receiver=recipient,
                msg_content=data['msg_content'],
            )
            message.save()

            return redirect('inbox:message_read_or_send', pk=conversation.id)
    else:
        form = ConversationForm()

    context = {
        'form': form,
        'recipient' : recipient
    }

    return render(request, 'inbox/conversation_form.html', context)







# @transaction.atomic
# def create_conversation2(request):
#     user = request.user

#     # import pdb; pdb.set_trace()

#     if request.method == "POST":
#         form = ConversationForm2(data=request.POST, user=user)
#         if form.is_valid():
#             data = form.cleaned_data

#             conversation = Conversation.objects.create(
#                 creator=user,
#                 recipient=data['recipient'],
#             )
#             conversation.save()
#             # import pdb; pdb.set_trace()
#             message = Message.objects.create(
#                 conversation=conversation,
#                 sender=user,
#                 receiver=recipient,
#                 msg_content=data['msg_content'],
#             )
#             message.save()

#             return redirect('inbox:message_read_or_send', pk=conversation.id)
#     else:
#         form = ConversationForm2(user=user)

#     return render(request, 'inbox/conversation_form.html', {'form':form})
