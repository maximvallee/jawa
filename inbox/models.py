from django.db import models
from accounts.models import User, Booking
from jawa.utils import get_time_ago
from datetime import date
from datetime import datetime as dt
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
from inbox.choices import *



# Create your models here.
class Conversation(models.Model):

    category = models.PositiveSmallIntegerField(choices=CONVERSATION_CATEGORY, default=1)
    creator = models.ForeignKey(User, related_name="creator", on_delete=models.CASCADE, null=True)
    recipient = models.ForeignKey(User, related_name="recipient", on_delete=models.CASCADE, null=True)
    updated = models.DateTimeField(auto_now_add=True)
    # is_read = models.BooleanField(default=False)

    # booking = models.OneToOneField(Booking, on_delete=models.CASCADE, null=True, related_name='booking')

    def __str__(self):
        return str(self.id)
    
    @property
    def is_read(self):
        # import pdb; pdb.set_trace()
        all_status_list = MessageStatus.objects.filter(message__in=self.message.all())
        unread_list = all_status_list.filter(is_read=False)
        if unread_list.exists():
            return False
        return True

    
    def get_last_message(self):
        last_message = self.message.all().order_by('-created')[0]
        return last_message
    
    #### To display how long ago the last message was sent
    def get_updated_ago(self):
        delta = dt.now(timezone.utc) - self.updated
        seconds = round(delta.total_seconds())
        updated_ago = get_time_ago(seconds)
        return updated_ago
    

class Message(models.Model):
    conversation = models.ForeignKey(Conversation, related_name="message", on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name="sender", on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="receiver", on_delete=models.CASCADE)
    msg_content = models.TextField(max_length=500, blank=True)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.msg_content[0:15] + "...")

    #### To display how long ago each message was created
    def get_updated_ago(self):
        delta = dt.now(timezone.utc) - self.created
        seconds = round(delta.total_seconds())
        updated_ago = get_time_ago(seconds)
        return updated_ago

    ### Each time a message is created, the parent conversation's timestamp is updated and pulled up in the feed.

    from django.db import transaction

    @transaction.atomic
    def save(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        conversation = self.conversation
        conversation.updated = dt.now(timezone.utc)
        conversation.save()
        super(Message, self).save(*args, **kwargs)
    
class MessageStatus(models.Model):
    message = models.OneToOneField(Message, on_delete=models.CASCADE, related_name='message_status')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False)
    read = models.DateTimeField(null=True)


class ConversationStatus(models.Model):
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE, related_name='conversation_status')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False)
    read = models.DateTimeField(null=True)


@receiver(post_save, sender=Message)
def auto_create_message_status_with_message(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    if kwargs['created']:

        ## Create message status
        message_status = MessageStatus.objects.create(
            message=instance,
            user=instance.receiver,
        )
        message_status.save()

        ## Update conversation status
        conversation_status = instance.conversation.conversation_status.all().get(user=instance.receiver) 
        conversation_status.is_read = False
        conversation_status.save()


@receiver(post_save, sender=Conversation)
def auto_create_conversation_status_with_conversation(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    if kwargs['created']:
        status_creator = ConversationStatus.objects.create(
            conversation=instance,
            user=instance.creator,
        )
        status_recipient = ConversationStatus.objects.create(
            conversation=instance,
            user=instance.recipient,
        )
        status_creator.save()
        status_recipient.save()