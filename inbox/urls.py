from django.contrib import admin
from django.urls import path, include
from inbox import views

app_name = 'inbox'

urlpatterns = [

    path('', views.list_conversations, name='conversation_list'),
    # path('conversation/<int:pk>', views.list_messages, name='message_list'),
    path('conversation/<int:pk>/messages', views.read_or_send_message, name='message_read_or_send'),
    path('conversation/create', views.create_conversation, name='conversation_create'),

    # path('conversation/create', views.create_conversation2, name='conversation_create2'),
    
]