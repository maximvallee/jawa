from django.contrib import admin
from inbox.models import *

# Register your models here.

admin.site.register(Conversation)
admin.site.register(Message)
admin.site.register(MessageStatus)
admin.site.register(ConversationStatus)