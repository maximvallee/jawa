"""jawa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import allauth

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('accounts/', include('accounts.urls')),
    path('', include("dashboard.urls", namespace="dashboard")),
    path('inbox/', include("inbox.urls", namespace="inbox")),
    path('invitations/', include('invitations.urls', namespace='invitations')),
    # path('hello/', include('allauth.urls')),
]

#     ^$ ^ ^signup/$ [name='account_signup']
#     ^$ ^ ^login/$ [name='account_login']
#     ^$ ^ ^logout/$ [name='account_logout']
#     ^$ ^ ^password/change/$ [name='account_change_password']
#     ^$ ^ ^password/set/$ [name='account_set_password']
#     ^$ ^ ^inactive/$ [name='account_inactive']
#     ^$ ^ ^email/$ [name='account_email']
#     ^$ ^ ^confirm-email/$ [name='account_email_verification_sent']
#     ^$ ^ ^confirm-email/(?P<key>[-:\w]+)/$ [name='account_confirm_email']
#     ^$ ^ ^password/reset/$ [name='account_reset_password']
#     ^$ ^ ^password/reset/done/$ [name='account_reset_password_done']
#     ^$ ^ ^password/reset/key/(?P<uidb36>[0-9A-Za-z]+) -(?P<key>.+)/$ [name='account_reset_password_from_key']
#     ^$ ^ ^password/reset/key/done/$ [name='account_reset_password_from_key_done']
#     ^$ ^social/
#     ^$ ^google/
#     ^$ ^facebook/
#     ^$ ^facebook/login/token/$ [name='facebook_login_by_token']
#     ^admin/
