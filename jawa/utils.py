from secrets import choice
from string import printable

def generate_password():
    length=15
    password = ''.join([choice(printable.strip()) for i in range(length)])
    return password

def get_time_ago(seconds):
    # import pdb; pdb.set_trace() 
    minutes = seconds//60
    hours = seconds//3600
    days = seconds//86400
    months = seconds//2592000
    
    if seconds <= 1:
        time_ago = {'value': seconds, 'units':'second'}  ### Seconds
    elif seconds < 60:
        time_ago = {'value': seconds, 'units':'seconds'}  ### Seconds
    elif minutes == 1:
        time_ago = {'value': minutes, 'units':'minute'}  ### Minute
    elif minutes < 60:
        time_ago = {'value': minutes, 'units':'minutes'}  ### Minutes
    elif hours == 1:
        time_ago = {'value': hours, 'units':'hour'} ### Hour
    elif hours  < 48:
        time_ago = {'value': hours, 'units':'hours'} ### Hours
    elif days < 30:
        time_ago = {'value': days, 'units': 'days'} ### Days
    elif months == 1:
        time_ago = {'value': months, 'units': 'month'} ## Month
    else:
        time_ago = {'value': months, 'units': 'months'} ## Months

    return time_ago