from django.core.exceptions import PermissionDenied
from accounts.models import User
from inbox.models import *
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist


"""
Forces user to select his organization upon sign up, name the 'organization' 
if user is a manager and select his 'user_type' if social account.
"""
def post_signup_required(function):
    def wrap(request, *args, **kwargs):
        # import pdb; pdb.set_trace()
        user = request.user

        if (user.user_type == 1 and not user.organization or 
                user.user_type == 2 and not user.organization or
                user.user_type == 3 and not user.organization):
            return redirect('accounts:post_signup_uninvited')
        elif user.user_type == 1 or user.user_type == 2 or user.user_type == 3: 
            if not user.first_name or not user.last_name:
                ## 2) Load form with name and last name (invited user)
                return redirect('accounts:post_signup_invitee')
        # elif user.user_type == 4 and user.organization.name == 'noname':
        elif user.user_type == 4 and not user.organization:
            ## 3) Load form with organization name, name, last name
            return redirect('accounts:post_signup_organization')
        ## For social accounts
        elif user.user_type == 0:
            return redirect('accounts:user_type_select')
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


"""
Prevents users to view a conversation they are not part of.
"""
def conversation_required(function):
    def wrap(request, *args, **kwargs):
        user = request.user
        pk = kwargs['pk']
        conversation = Conversation.objects.get(id=pk)
        creator = conversation.creator
        recipient = conversation.recipient

        # import pdb; pdb.set_trace()
        if request.user != creator and request.user != recipient:
            return redirect('/')
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


"""
Prevents children users (ie, tenant, landlord, merchant) from accessing
"""
def parent_only(user):
    return user.user_type == 4

"""
Prevents parent users (ie, organization) from accessing
"""
def children_only(user):
    return user.user_type == 1 or user.user_type == 2 or user.user_type == 3




## V2
# def organization_required(function):
#     def wrap(request, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         if request.user.user_type == 1 and not request.user.tenant_profile.organization:
#                 return redirect('user:organization_select', pk=request.user.pk)
#         elif request.user.user_type == 2 and not request.user.owner_profile.organization:
#                 return redirect('user:organization_select', pk=request.user.pk)
#         elif request.user.user_type == 3 and not request.user.organization_profile.name:
#                 return redirect('user:organization_name', pk=request.user.pk)
#         elif request.user.user_type == 4 and not request.user.merchant_profile.organizations.exists():
#                 return redirect('user:organization_select', pk=request.user.pk)
#         else:
#             return function(request, *args, **kwargs)
#         return render(request, 'dashboard/dashboard.html')
        
#     wrap.__doc__ = function.__doc__
#     wrap.__name__ = function.__name__
#     return wrap




###

## V1
# def organization_required(function):
#     def wrap(request, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         if request.user.user_type == 1:
#             if not request.user.tenant_profile.organization:
#                 return redirect('user:organization_select', pk=request.user.pk)
#             else:
#                 return function(request, *args, **kwargs)
#         if request.user.user_type == 2:
#             if not request.user.owner_profile.organization:
#                 return redirect('user:organization_select', pk=request.user.pk)
#             else:
#                 return function(request, *args, **kwargs)
#         if request.user.user_type == 3:
#             if not request.user.organization_profile.name:
#                 return redirect('user:organization_name', pk=request.user.pk)
#             else:
#                 return function(request, *args, **kwargs)
#         if request.user.user_type == 4:
#             # import pdb; pdb.set_trace()
#             if not request.user.merchant_profile.organizations:
#                 return redirect('user:organization_select', pk=request.user.pk)
#             else:
#                 return function(request, *args, **kwargs)
#         return render(request, 'dashboard/dashboard.html')
        
#     wrap.__doc__ = function.__doc__
#     wrap.__name__ = function.__name__
#     return wrap







# def profile_required(function):
#     def wrap(request, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         if request.user.has_usable_password():
#             return function(request, *args, **kwargs)
#         else:
#             # raise PermissionDenied
#             return redirect('user:password_create', pk=request.user.pk)
#     wrap.__doc__ = function.__doc__
#     wrap.__name__ = function.__name__
#     return wrap


# def user_type_required(function):
#     def wrap(request, *args, **kwargs):
#         # import pdb; pdb.set_trace()
#         if request.user.user_type == 0:
#             return redirect('user:user_type_form.html', pk=request.user.pk)
#         return function(request, *args, **kwargs)
#     wrap.__doc__ = function.__doc__
#     wrap.__name__ = function.__name__
#     return wrap