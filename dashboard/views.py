from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from jawa.decorators import post_signup_required
from invitations.utils import get_invitation_model
from accounts.models import *




def home(request):
    # import pdb; pdb.set_trace()
    user = request.user
    context = {
        'user': user
    }
    return render(request, "dashboard/index.html", context)

from inbox.models import *
@login_required
@post_signup_required
def dashboard(request):
    # import pdb; pdb.set_trace()
    
    pending_events = 0
    user = request.user

    ## All bookings associated to organization
    if user.user_type == 4:
        Invitation = get_invitation_model()
        no_invitations = len(Invitation.objects.filter(inviter=user, accepted=False))
        # import pdb; pdb.set_trace()
        organization = user.organization
        # no_bookings = len(Booking.objects.filter(building__landlord__organization = organization, status=0))
        no_bookings = len(Booking.objects.filter(building__landlord__user__organization=organization, status=0))

        pending_events = no_invitations + no_bookings

    elif user.user_type == 1:
        pending_events = len(Booking.objects.filter(tenant=user.tenant_profile, status=0))

    # import pdb; pdb.set_trace()
    inbox = len(ConversationStatus.objects.filter(user=user, is_read=False)) 


    # if user.user_type == 1:
    #     organization = request.user.tenant_profile.organization
    # elif request.user.user_type == 2:
    #     organization = request.user.landlord_profile.organization
    # elif request.user.user_type == 3:
    #     organization = request.user.merchant_profile.organizations
    # elif request.user.user_type == 4:
    #     organization = request.user.organization.name

    manager = ManagerProfile.objects.filter(user__organization=user.organization)[0]

    # import pdb; pdb.set_trace()
    context = {
        'pending_events':pending_events,
        'inbox': inbox,
        # 'organization': organization
        'manager' : manager
    }

    return render(request, "dashboard/dashboard.html", context)


def list_listings(request, slug):

    # import pdb; pdb.set_trace()
    organization = OrganizationProfile.objects.get(slug=slug)
    buildings = Building.objects.filter(landlord__user__organization=organization)
    context= {
        'organization': organization,
        'buildings': buildings
    }

    return render(request, 'dashboard/listing_list.html', context)

def listing(request, slug, pk):
    # import pdb; pdb.set_trace()
    organization = OrganizationProfile.objects.get(slug=slug)
    buildings = Building.objects.filter(landlord__user__organization=organization)
    building = buildings.get(id=pk)
    listings = building.rental_unit.all().order_by('unit_type')

    context= {
        'organization': organization,
        'building': building,
        'listings': listings
    }

    return render(request, 'dashboard/listing.html', context)

@login_required
def check_out(request, slug, pk):
    # import pdb; pdb.set_trace()
    organization = OrganizationProfile.objects.get(slug=slug)
    listings = RentalUnit.objects.filter(apartment__landlord__user__organization=organization)
    listing = listings.get(id=pk)


    context= {
        'organization': organization,
        'listing': listing
    }

    return render(request, 'dashboard/checkout.html', context)