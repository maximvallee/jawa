from django.urls import path
from django.contrib.auth import views as auth_views
from dashboard import views
from django.urls import reverse_lazy

app_name = 'dashboard'

urlpatterns = [
    # path('', views.Home.as_view(), name='home'),
    path('', views.home, name='home'),
    path('dashboard/', views.dashboard, name='dashboard'),
    # path('<str:slug>/dashboard', views.dashboard, name='dashboard'),
    path('organization/<str:slug>', views.list_listings, name='listing_list'),
    path('organization/<str:slug>/listing/<str:pk>', views.listing, name='listing'),
    path('organization/<str:slug>/checkout/<str:pk>', views.check_out, name='check_out'),

    
]
