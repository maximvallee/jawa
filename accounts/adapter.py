from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.shortcuts import resolve_url


class MyAccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request):
        # import pdb; pdb.set_trace()
        # if self.request.user.user_type == 0:
        #     path="/user/{user_id}/completion/".format(user_id=self.request.user.id)
        # else:
        #     path="/"
        path = "/"
        return resolve_url(path)

    # def get_signup_redirect_url(self, request):
    #     # import pdb; pdb.set_trace()
    #     if self.request.user.user_type == 0:
    #         path="/user/{user_id}/completion/".format(user_id=self.request.user.id)
    #     else:
    #         path="/"
    #     return resolve_url(path)

        def get_signup_redirect_url(self, request):
            path="/"
        return resolve_url(path)
    
    # def save_user(self, request, user, form, commit=True):
    #     """
    #     This is called when saving user via allauth registration.
    #     We override this to set additional data on user object.
    #     """
    #     # Do not persist the user yet so we pass commit=False
    #     # (last argument)
    #     import pdb; pdb.set_trace()
    #     user = super(UserAccountAdapter, self).save_user(request, user, form, commit=False)
    #     user.age = form.cleaned_data.get('age')
    #     user.save()

class MySocialAccountAdapter(DefaultSocialAccountAdapter):

    def get_login_redirect_url(self, request):
        import pdb; pdb.set_trace()
        path = "https://www.rentaplacenow.com/ottawa?q=kilo"
        return path