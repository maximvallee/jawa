from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_user_type(value):
    if not 0 <= value <= 4:
        raise ValidationError(
            _('%(value)s is not a valid user type option'),
            params={'value': value},
        )