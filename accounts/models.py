from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from invitations.utils import get_invitation_model
from django.core.exceptions import ObjectDoesNotExist
from datetime import date
from datetime import datetime as dt
from jawa.utils import get_time_ago
from django.template.defaultfilters import slugify
from accounts.choices import *
from multiselectfield import MultiSelectField

# from inbox.models import *
# from inbox.models import *



class OrganizationProfile(models.Model):
    name = models.CharField(max_length=45)
    slug = models.SlugField(null=False, unique=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

class User(AbstractUser):
    USER_TYPE_CHOICES  = [
        (0, ''), ## Necessary for social signups
        (1, 'tenant'),
        (2, 'landlord'),
        (3, 'merchant'),
        (4, 'property manager'),
    ]

    PROVINCES_CHOICES= [
        ('AB', 'Alberta'),
        ('BC', 'British Columbia'),
        ('MB', 'Manitoba'),
        ('NB', 'New Brunswick'),
        ('NL', 'Newfoundland and Labrador'),
        ('NT', 'Northwest Territories'),
        ('NS', 'Nova Scotia'),
        ('NU', 'Nunavut'),
        ('ON', 'Ontario'),
        ('NB', 'Prince Edward Island'),
        ('QC', 'Quebec'),
        ('SK', 'Saskatchewan'),
        ('YT', 'Yukon'),
    ]

    COUNTRIES_CHOICES= [
        ('CA','Canada'),
    ]

    ACCOUNT_TYPES = [
        ('CA','Canadian'),
    ]

    YEARS_CHOICES= [
        ('', '--'),
        (abs(date.today().year) % 100, date.today().year),
        (abs(date.today().year+1) % 100, date.today().year +1),
        (abs(date.today().year+2) % 100, date.today().year +2),
        (abs(date.today().year+3) % 100, date.today().year +3),
        (abs(date.today().year+4) % 100, date.today().year +4),
        (abs(date.today().year+5) % 100, date.today().year +5),
        (abs(date.today().year+6) % 100, date.today().year +6),
        (abs(date.today().year+7) % 100, date.today().year +7),
        (abs(date.today().year+8) % 100, date.today().year +8),
        (abs(date.today().year+9) % 100, date.today().year +9),
        (abs(date.today().year+10) % 100, date.today().year +10),
    ]

    MONTHS_CHOICES= [
        ('', '--'),
        ('01', '01'),
        ('02', '02'),
        ('03', '03'),
        ('04', '04'),
        ('05', '05'),
        ('06', '06'),
        ('07', '07'),
        ('08', '08'),
        ('09', '09'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12')
    ]

    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, default=0)
    organization = models.ForeignKey(OrganizationProfile, related_name='user', null=True, on_delete=models.SET_NULL)
    phone = models.CharField(max_length=18, blank=True)

    bank_account_holder = models.CharField(max_length=50, blank=True)
    bank_account_type = models.CharField(max_length=2, choices=ACCOUNT_TYPES, default='CA', blank=True)
    institution_number = models.CharField(max_length=3, blank=True)
    branch_number = models.CharField(max_length=5, blank=True)
    account_number= models.CharField(max_length=12, blank=True)

    card_name = models.CharField(max_length=50, blank=True)
    card_number = models.CharField(max_length=16, blank=True)
    card_expiry_month = models.CharField(max_length=2, choices=MONTHS_CHOICES, blank=True)
    card_expiry_year = models.CharField(max_length=2, choices=YEARS_CHOICES, blank=True)
    card_cvd = models.CharField(max_length=5, blank=True)

    def get_absolute_url(self):
        return reverse('home')
    
    def __str__(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        return self.username

class ManagerProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='manager_profile')

    def __str__(self):
        return str(self.user)

class TenantProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='tenant_profile')

    def __str__(self):
        return str(self.user)

class LandlordProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='landlord_profile')

    def __str__(self):
        return str(self.user)

class MerchantProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='merchant_profile')

    def __str__(self):
        return str(self.user)



class Building(models.Model):

    # def __init__(self, *args, **kwargs):
    #     super(Building, self).__init__(*args, **kwargs)
    #     self.__original_category = self.category

    landlord = models.ForeignKey('LandlordProfile', on_delete=models.CASCADE)
    address = models.CharField(max_length=50)
    unit = models.CharField(max_length=5, blank=True)
    size = models.PositiveSmallIntegerField(choices=UNIT_SIZE_OPTIONS)
    bath = models.PositiveSmallIntegerField()
    rental_type = models.PositiveSmallIntegerField(choices=RENTAL_OPTIONS)
    # rental_type = MultiSelectField(choices=RENTAL_OPTIONS)

    def __str__(self):
        if self.unit:
            return str("{}-{}".format(self.unit, self.address))
        return self.address


@receiver(post_save, sender=Building)
def auto_create_unit_with_building(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    ## Building is created V1
    # if kwargs['created']:
    #     if instance.category == 1:
    #         RentalUnit.objects.create(apartment=instance, room_no='')
    #     else:
    #         # import pdb; pdb.set_trace()
    #         units = [RentalUnit(apartment=instance, room_no=j) for j in range(1, instance.size + 1)]
    #         RentalUnit.objects.bulk_create(units)

    if kwargs['created']:
        rooms = [RentalUnit(apartment=instance, room_no=j, unit_type=2) for j in range(1, instance.size + 1)]
        unit = [RentalUnit(apartment=instance, room_no='', unit_type=1)]
        import pdb; pdb.set_trace()
        all_units = rooms + unit
        RentalUnit.objects.bulk_create(all_units)




    ## Building is updated
    # else:
    #     print('update to building')
    #     if instance.category != instance._Building__original_category:
    #         print('specific update to category')
    #         RentalUnit.objects.filter(apartment=instance).delete()
    #         print('deleting all existing listings')
    #         import pdb; pdb.set_trace()
    #         if instance.category == 1:
                
    #             print('creating 1 listing')
    #             RentalUnit.objects.create(apartment=instance, room_no='')
    #         else:
    #             print('creating {} listings'.format(instance.size))
    #             units = [RentalUnit(apartment=instance, room_no=j) for j in range(1, instance.size + 1)]
    #             import pdb; pdb.set_trace()
    #             RentalUnit.objects.bulk_create(units)
    #             instance.size = len(RentalUnit.objects.filter(apartment=instance))
    #             instance.save()








class RentalUnit(models.Model):
    apartment = models.ForeignKey('Building', related_name='rental_unit', on_delete=models.CASCADE)
    unit_type = models.PositiveSmallIntegerField(choices=UNIT_TYPE)
    room_no = models.CharField(max_length=5, blank=True)
    
    def __str__(self):
        if self.unit_type == 2:
        # if self.apartment.category == 2: V1
        # if self.room_no: V0
            return str("{} · Room {}".format(self.apartment, self.room_no))
        return str("{} · Entire unit".format(self.apartment))

# @receiver(post_delete, sender=RentalUnit) ## V1
# def auto_update_unit_size_when_delete(sender, instance, **kwargs):
#     # import pdb; pdb.set_trace()
#     instance.apartment.size = len(RentalUnit.objects.filter(apartment=instance.apartment))
#     # if instance.apartment.size == 1:
#     #     instance.apartment.category = 1
#     instance.apartment.save()

# @receiver(post_save, sender=RentalUnit)
# def auto_update_unit_size_when_create(sender, instance, **kwargs):
#     # import pdb; pdb.set_trace()
#     instance.apartment.size = len(RentalUnit.objects.filter(apartment=instance.apartment))
#     instance.apartment.save()


@receiver(post_delete, sender=RentalUnit)
def auto_update_unit_size_when_delete(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    instance.apartment.size = len(RentalUnit.objects.filter(apartment=instance.apartment, unit_type=2))
    instance.apartment.save()

@receiver(post_save, sender=RentalUnit)
def auto_update_unit_size_when_create(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    instance.apartment.size = len(RentalUnit.objects.filter(apartment=instance.apartment, unit_type=2))
    instance.apartment.save()

class Booking(models.Model):
    STATUS_OPTIONS = [
        (0, 'Pending'),
        (1, 'Accepted'),
        (2, 'In-tenancy'),
        (3, 'Moved-out'),
        (4, 'Cancelled'),
    ]
    status = models.PositiveSmallIntegerField(choices=STATUS_OPTIONS, default=0)
    tenant = models.ForeignKey('TenantProfile', on_delete=models.CASCADE, related_name='booking')
    building = models.ForeignKey('Building', on_delete=models.CASCADE)
    check_in = models.DateField(blank=False)
    check_out = models.DateField(blank=False)
    rent = models.DecimalField(max_digits=6, decimal_places=2, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return str('{} - {} ({})'.format(self.check_in, self.check_out,self.tenant))
    
    def get_model_name(self):
        model_name = 'Booking'
        return model_name 
    
    def get_create_delta(self):
        delta = dt.now(timezone.utc) - self.created
        seconds = round(delta.total_seconds())
        time_ago = get_time_ago(seconds)
        return time_ago
        
        # import pdb; pdb.set_trace()







"""
Auto-delete 'user' if the associated 'profile' is deleted. This is to supplement
the existing CASCADE default to auto-delete 'profile' if 'user' is deleted.
"""

@receiver(post_delete, sender=TenantProfile)
def auto_delete_user_with_tenant_profile(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    instance.user.delete()

@receiver(post_delete, sender=LandlordProfile)
def auto_delete_user_with_landlord_profile(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    instance.user.delete()

@receiver(post_delete, sender=MerchantProfile)
def auto_delete_user_with_merchant_profile(sender, instance, **kwargs):
    instance.user.delete()

@receiver(post_delete, sender=ManagerProfile)
def auto_delete_user_with_manager_profile(sender, instance, **kwargs):
    instance.user.delete()

@receiver(post_delete, sender=User)
def auto_delete_invitation_with_user(sender, instance, **kwargs):
    try:
        Invitation = get_invitation_model()
        invite = Invitation.objects.get(email=instance.email)
        invite.delete()
    except ObjectDoesNotExist:
        pass


"""
Override django-invitations model to add 'user_type' field
"""

import datetime
from django.conf import settings
from django.contrib.sites.models import Site
try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from invitations import signals
from invitations.adapters import get_invitations_adapter
from invitations.app_settings import app_settings
from invitations.base_invitation import AbstractBaseInvitation

class CustomInvitation(AbstractBaseInvitation):
    USER_TYPE_CHOICES  = [
        (1, 'Tenant'),
        (2, 'Landlord'),
        (3, 'Merchant'),
        (4, 'Property manager'),
    ]

    email = models.EmailField(unique=True, verbose_name=_('e-mail address'),
                              max_length=app_settings.EMAIL_MAX_LENGTH)
    created = models.DateTimeField(verbose_name=_('created'),
                                   default=timezone.now)
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, default=1)

    def get_model_name(self):
        model_name = 'Invitation'
        return model_name 
    
    def get_create_delta(self):
        delta = dt.now(timezone.utc) - self.created
        seconds = round(delta.total_seconds())
        time_ago = get_time_ago(seconds)
        return time_ago

    @classmethod
    def create(cls, email, user_type, inviter=None, **kwargs):
        key = get_random_string(64).lower()
        instance = cls._default_manager.create(
            email=email,
            key=key,
            inviter=inviter,
            user_type=user_type,
            **kwargs)
        return instance

    def key_expired(self):
        expiration_date = (
            self.sent + datetime.timedelta(
                days=app_settings.INVITATION_EXPIRY))
        return expiration_date <= timezone.now()

    def send_invitation(self, request, **kwargs):
        current_site = kwargs.pop('site', Site.objects.get_current())
        invite_url = reverse('invitations:accept-invite',
                             args=[self.key])
        invite_url = request.build_absolute_uri(invite_url)
        ctx = kwargs
        ctx.update({
            'invite_url': invite_url,
            'site_name': current_site.name,
            'email': self.email,
            'key': self.key,
            'inviter': self.inviter,
        })

        email_template = 'invitations/email/email_invite'

        get_invitations_adapter().send_mail(
            email_template,
            self.email,
            ctx)
        self.sent = timezone.now()
        self.save()

        signals.invite_url_sent.send(
            sender=self.__class__,
            instance=self,
            invite_url_sent=invite_url,
            inviter=self.inviter)

    def __str__(self):
        return "Invite: {0}".format(self.email)


# here for backwards compatibility, historic allauth adapter
if hasattr(settings, 'ACCOUNT_ADAPTER'):
    if settings.ACCOUNT_ADAPTER == 'invitations.models.InvitationsAdapter':
        from allauth.account.adapter import DefaultAccountAdapter
        from allauth.account.signals import user_signed_up

        class InvitationsAdapter(DefaultAccountAdapter):

            def is_open_for_signup(self, request):
                if hasattr(request, 'session') and request.session.get(
                        'account_verified_email'):
                    return True
                elif app_settings.INVITATION_ONLY is True:
                    # Site is ONLY open for invites
                    return False
                else:
                    # Site is open to signup
                    return True

            def get_user_signed_up_signal(self):
                return user_signed_up












# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, created, **kwargs):                           <<<<<<<<---------- IMPORTANT
#     if created:
#         try:
#             # import pdb; pdb.set_trace()
#             print('model')
#             Invitation = get_invitation_model()
#             invite = Invitation.objects.get(email=instance.email)
#             organization = invite.inviter.manager_profile
#             instance.user_type = invite.user_type
#             instance.save()
#             if instance.user_type == 1:
#                 TenantProfile.objects.create(user=instance, manager=organization)
#             elif instance.user_type == 2:
#                 OwnerProfile.objects.create(user=instance, manager=organization)
#             print("this user was invited")

#         except ObjectDoesNotExist:
#             if instance.user_type == 1:
#                 TenantProfile.objects.create(user=instance)
#             elif instance.user_type == 2:
#                 OwnerProfile.objects.create(user=instance)
#             # TenantProfile.objects.create(user=instance)
#             print("this user was not invited")
        






# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, created, **kwargs):
#     if created:
#         try:
#             import pdb; pdb.set_trace()
#             Invitation = get_invitation_model()
#             invitee = Invitation.objects.get(email=instance.email)
#             TenantProfile.objects.create(user=instance, manager=invitee.inviter.manager_profile)
#         except ObjectDoesNotExist:
#             TenantProfile.objects.create(user=instance)





        

    # if created:
    #     if instance.user_type == 1:
    #         try:
    #             invitee = Invitation.objects.get(email=instance.email)
    #             import pdb; pdb.set_trace()
    #             TenantProfile.objects.create(user=instance, manager=invitee.inviter)
    #         except:
    #             TenantProfile.objects.create(user=instance)
        
    #     # TenantProfile.objects.create(user=instance, manager=instance.created_by.manager_profile)
    # else:
    #     if instance.user_type == 1:
    #         TenantProfile.objects.get(user=instance).manager = instance.created_by.manager_profile

    


    #     TenantProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
    #     # TenantProfile.objects.update_or_create(user=instance, manager=instance.created_by.manager_profile)
    # elif instance.user_type == 2:
    #     OwnerProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
    # elif instance.user_type == 3:
    #     ManagerProfile.objects.get_or_create(user=instance)
    # elif instance.user_type == 4:
    #     VendorProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     import pdb; pdb.set_trace()
#     if instance.user_type == 1:
#         TenantProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
#         # TenantProfile.objects.update_or_create(user=instance, manager=instance.created_by.manager_profile)
#     elif instance.user_type == 2:
#         OwnerProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
#     elif instance.user_type == 3:
#         ManagerProfile.objects.get_or_create(user=instance)
#     elif instance.user_type == 4:
#         VendorProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)






# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     import pdb; pdb.set_trace()
#     if instance.user_type == 1:
#         TenantProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
#         # TenantProfile.objects.update_or_create(user=instance, manager=instance.created_by.manager_profile)
#     elif instance.user_type == 2:
#         OwnerProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)
#     elif instance.user_type == 3:
#         ManagerProfile.objects.get_or_create(user=instance)
#     elif instance.user_type == 4:
#         VendorProfile.objects.get_or_create(user=instance, manager=instance.created_by.manager_profile)


	# print(instance.internprofile.bio, instance.internprofile.location)
	# if instance.is_intern:
	# 	instance.intern_profile.save()
	# else:
	# 	HRProfile.objects.get_or_create(user = instance)