# Generated by Django 3.1.3 on 2020-12-12 01:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20201211_1901'),
    ]

    operations = [
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('room_no', models.CharField(blank=True, max_length=5)),
            ],
        ),
        migrations.AddField(
            model_name='building',
            name='category',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Entire unit'), (2, 'Private room')], default=1),
        ),
        migrations.AddField(
            model_name='building',
            name='unit',
            field=models.CharField(blank=True, max_length=5),
        ),
        migrations.DeleteModel(
            name='RentalUnit',
        ),
        migrations.AddField(
            model_name='room',
            name='apartment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.building'),
        ),
    ]
