# Generated by Django 3.1.3 on 2020-12-12 00:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='building',
            name='unit',
        ),
        migrations.CreateModel(
            name='RentalUnit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit', models.CharField(blank=True, max_length=5)),
                ('category', models.PositiveSmallIntegerField(choices=[(1, 'Entire unit'), (2, 'Private room')], default=1)),
                ('building', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.building')),
            ],
        ),
    ]
