from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from invitations.utils import get_invitation_model
from django.core.exceptions import ObjectDoesNotExist
from accounts.models import *



class TenantProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user']

class OrganizationProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    prepopulated_fields = {'slug': ('name',)}


class BuildingAdmin(admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            # import pdb; pdb.set_trace()
            return self.readonly_fields + ('size',)
        return self.readonly_fields

class RentalUnitAdmin(admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        if obj: 
            fields = ['unit_type',]
            if obj.unit_type == 1:
                fields.append('room_no')
            return self.readonly_fields + tuple(fields)
        return self.readonly_fields



class UserAdmin(admin.ModelAdmin):
    list_display = ['id','username', 'email', 'user_type', ]

    """
    Admin can only select user_type when creating obj. Not possible to
    update.
    """ 
    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('user_type',)
        return self.readonly_fields

    def save_model(self, request, obj, form, change):
        super(UserAdmin, self).save_model(request, obj, form, change)
        import pdb; pdb.set_trace()
        try:
            """
            USER INVITED
            Invitation links orgnanization (inviter) with user created.
            """ 
            Invitation = get_invitation_model()
            invite = Invitation.objects.get(email=obj.email)
            organization = invite.inviter.organization_profile
            obj.user_type = invite.user_type
            obj.save()

            if obj.user_type == 1:
                TenantProfile.objects.update_or_create(user=obj, organization=organization)
            elif obj.user_type == 2:
                LandlordProfile.objects.update_or_create(user=obj, organization=organization)
            elif obj.user_type == 3:
                merchant_obj = MerchantProfile.objects.update_or_create(user=obj)
                merchant_obj.organizations.add(organization)  
        
            """
            USER NOT INVITED
            User default sign in (email, password, type) in forms.py but missing
            organization link. Will be prompted to provide organization when logging
            in first time (decorator "post_signup_required")
            """ 
        except ObjectDoesNotExist:

            if obj.user_type == 1:
                TenantProfile.objects.update_or_create(user=obj)
            elif obj.user_type == 2:
                LandlordProfile.objects.update_or_create(user=obj)
            elif obj.user_type == 3:
                MerchantProfile.objects.update_or_create(user=obj)
            elif obj.user_type == 4:
                OrganizationProfile.objects.update_or_create(user=obj)

class BookingAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'tenant', 'building', 'created', 'updated']

admin.site.register(User,UserAdmin)
admin.site.register(TenantProfile, TenantProfileAdmin)
admin.site.register(LandlordProfile)
admin.site.register(OrganizationProfile, OrganizationProfileAdmin)
admin.site.register(ManagerProfile)
admin.site.register(MerchantProfile)
admin.site.register(Building, BuildingAdmin)
# admin.site.register(Building)
admin.site.register(RentalUnit, RentalUnitAdmin)
admin.site.register(Booking, BookingAdmin)
