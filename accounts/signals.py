from allauth.account.signals import user_signed_up
from django.dispatch import receiver
from invitations.utils import get_invitation_model
from accounts.models import TenantProfile, LandlordProfile, OrganizationProfile, MerchantProfile, ManagerProfile
from django.core.exceptions import ObjectDoesNotExist


# @receiver(user_signed_up)
# def user_signed_up(request, user, **kwargs):
#     # import pdb; pdb.set_trace()
#     try:
#         """
#         IF THERE IS INVITATION:
#         User's organization is provided in the invitation and saved in the profile.
#         """ 
#         Invitation = get_invitation_model()
#         invite = Invitation.objects.get(email=user.email)
#         organization = invite.inviter.organization_profile
#         user.user_type = invite.user_type
#         user.save()

#         if user.user_type == 1:
#             TenantProfile.objects.update_or_create(user=user, organization=organization)
#         elif user.user_type == 2:
#             LandlordProfile.objects.update_or_create(user=user, organization=organization)
#         elif user.user_type == 3:
#             merchant_obj = MerchantProfile.objects.update_or_create(user=user)[0]
#             merchant_obj.organizations.add(organization)

@receiver(user_signed_up)
def user_signed_up(request, user, **kwargs):
    # import pdb; pdb.set_trace()
    try:
        """
        IF THERE IS INVITATION:
        User's organization is provided in the invitation and saved in the profile.
        """ 
        Invitation = get_invitation_model()
        invite = Invitation.objects.get(email=user.email)
        organization = invite.inviter.organization
        user.user_type = invite.user_type
        user.organization = organization
        user.save()

        if user.user_type == 1:
            TenantProfile.objects.update_or_create(user=user)
        elif user.user_type == 2:
            LandlordProfile.objects.update_or_create(user=user)
        elif user.user_type == 3:
            MerchantProfile.objects.update_or_create(user=user)[0]
            

    except ObjectDoesNotExist:
        """
        IF THERE IS NO INVITATION (STANDARD; NOT SOCIAL ACCOUNT):
        User Will be prompted to provide organization when logging
        in first time (decorator "post_signup_required")
        """ 
        if user.user_type == 1:
            TenantProfile.objects.update_or_create(user=user)
        elif user.user_type == 2:
            LandlordProfile.objects.update_or_create(user=user)
        elif user.user_type == 3:
            MerchantProfile.objects.update_or_create(user=user)
        elif user.user_type == 4:
            # organization = OrganizationProfile.objects.create(name="noname")
            # user.organization = organization
            # user.save()
            ManagerProfile.objects.update_or_create(user=user)










# from allauth.account.views import PasswordResetView

# from django.conf import settings
# from django.dispatch import receiver
# from django.http import HttpRequest
# from django.middleware.csrf import get_token
# from django.db.models.signals import post_save


# from django.contrib.auth.models import User
# from django.db.models.signals import post_save


# from cmdbox.profiles.models import Profile

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)


## INVITATIONS



        # print("this user was NOT invited")


        # try:
        #     # import pdb; pdb.set_trace()
        #     print('model')
        #     Invitation = get_invitation_model()
        #     invite = Invitation.objects.get(email=instance.email)
        #     organization = invite.inviter.manager_profile
        #     instance.user_type = invite.user_type
        #     instance.save()
        #     if instance.user_type == 1:
        #         TenantProfile.objects.create(user=instance, manager=organization)
        #     elif instance.user_type == 2:
        #         OwnerProfile.objects.create(user=instance, manager=organization)
        #     print("this user was invited")

        # except ObjectDoesNotExist:
        #     if instance.user_type == 1:
        #         TenantProfile.objects.create(user=instance)
        #     elif instance.user_type == 2:
        #         OwnerProfile.objects.create(user=instance)
        #     # TenantProfile.objects.create(user=instance)
        #     print("this user was not invited")






    # else:
    #     print("this was an invited user")
        # user.company = invite.inviter.company
        # user.save()

# @receiver(user_signed_up)
# def user_signed_up(request, user, **kwargs):
#     import pdb; pdb.set_trace()
    
#     if request.user.is_authenticated: ## If created by Parent User
#         user.created_by = request.user

#         user.set_unusable_password()
#         user.save()




# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def send_reset_password_email(sender, instance, created, **kwargs):
#     import pdb; pdb.set_trace()
#     if created:
#         print('created')
#         # First create a post request to pass to the view
#         request = HttpRequest()
#         request.method = 'POST'

#         # add the absolute url to be be included in email
#         if settings.DEBUG:
#             request.META['HTTP_HOST'] = '127.0.0.1:8000'
#         else:
#             request.META['HTTP_HOST'] = 'www.mysite.com'

#         # pass the post form data
#         request.POST = {
#             'email': instance.email,
#             'csrfmiddlewaretoken': get_token(HttpRequest())
#         }
#         PasswordResetView.as_view()(request)  # email will be sent!