from django.contrib import admin
from django.urls import path, include
from accounts import views
import inbox.views
# from inbox.views import create_conversation


app_name = 'accounts'

urlpatterns = [

    path('signup/completion/no-invite', views.post_signup_uninvited, name='post_signup_uninvited'),
    path('signup/completion/invite', views.post_signup_invitee, name='post_signup_invitee'),
    path('signup/completion/organization', views.post_signup_organization, name='post_signup_organization'),
    path('signup/user-type/select', views.select_user_type, name='user_type_select'),
    path('invitations/send', views.invite_user, name='user_invite'),
    path('invitations/<int:pk>/resend', views.resend_invite, name='invite_resend'),
    path('invitations/signup/', views.invitee_signup, name='signup_invitee'),
    path('settings/', views.settings, name='settings'),
    path('settings/banking/update', views.update_bank_account, name='banking_update'),
    path('settings/profile/update', views.update_profile, name='profile_update'),
    path('settings/deactivate/confirm', views.account_confirm_deactivate, name='account_confirm_deactivate'),

    path('<str:username>/update', views.update_user, name='user_update'),
    path('<str:username>/message', inbox.views.create_conversation, name='conversation_create'), ###

    path('landlords', views.list_profiles, name='landlord_list'),
    path('tenants', views.list_profiles, name='tenant_list'),
    path('merchants', views.list_profiles, name='merchant_list'),



    path('buildings/create', views.create_building, name='building_create'),
    path('buildings/<int:pk>/update', views.update_building, name='building_update'),
    path('buildings/<int:pk>/delete', views.delete_building, name='building_delete'),
    path('buildings', views.list_buildings, name='building_list'),

    path('bookings/create', views.create_booking, name='booking_create'),
    path('bookings', views.list_bookings, name='booking_list'),

    path('notifications', views.notifications, name='notifications'),

    
    # path('<int:pk>/create_password/', views.create_password, name='password_create'),
    # path('create/', views.create_user, name='user_create'),
    # path('new_user/',views.new_user_signup,name='new_user_signup'),
    # path('delete/account/', views.delete_account, name='delete_account'),
    # path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # path('<int:pk>/completion/', views.select_user_type, name='user_type_select'),
    # path("hello/", allauth.account.views.PasswordChangeView, name=""),
]


