from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from accounts.models import *
from accounts.validation import validate_user_type
from allauth.account.forms import SignupForm


class UserTypeSelectForm(forms.Form):

    USER_TYPES  = [
        (1, 'tenant'),
        (2, 'landlord'),
        (3, 'merchant'),
        (4, 'property manager'),
    ]

    user_type = forms.IntegerField(validators=[validate_user_type], widget=forms.Select(choices= USER_TYPES))

class UninvitedPostSignupForm(forms.Form):

    first_name = forms.CharField(max_length=45)
    last_name = forms.CharField(max_length=45)
    
    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        super(UninvitedPostSignupForm, self).__init__(*args, **kwargs)
        self.fields['organization'] = forms.ChoiceField(
                choices=[(organization.id, str(organization)) for organization in OrganizationProfile.objects.all()]
            )

class InviteePostSignupForm(forms.Form):

    first_name = forms.CharField(max_length=45)
    last_name = forms.CharField(max_length=45)


    
    # def __init__(self, *args, **kwargs):
    #     # import pdb; pdb.set_trace()
    #     request = kwargs.pop('request')
    #     user_type = request.user.user_type
    #     super(SelectOrganizationForm, self).__init__(*args, **kwargs)
  
    #     if user_type == 3:
    #         self.fields['organization'] = forms.MultipleChoiceField(
    #             choices=[(organization.id, str(organization)) for organization in OrganizationProfile.objects.all()]
    #         )
    #     else:
    #         self.fields['organization'] = forms.ChoiceField(
    #             choices=[(organization.id, str(organization)) for organization in OrganizationProfile.objects.all()]
    #         )
        
class OrganizationPostSignupForm(forms.Form):
    
    organization_name = forms.CharField(min_length=7, max_length=45, label="Name of the organization")
    contact_first_name = forms.CharField(min_length=2, max_length=45, label="First name")
    contact_last_name = forms.CharField(min_length=2, max_length=45, label="Last name")

class UserInviteForm(forms.Form):

    USER_TYPES = [
        (1,'tenant'),
        (2,'landlord'),
        (3,'merchant')
    ]

    email = forms.EmailField(label='Email', max_length=45)
    user_type = forms.IntegerField(validators=[validate_user_type], widget=forms.Select(choices=USER_TYPES))

class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', max_length=45)
    last_name = forms.CharField(label='Last Name', max_length=45)

    class Meta():
        model = User
        fields = 'first_name', 'last_name'

class ProfileUpdateForm(forms.ModelForm):

    first_name = forms.CharField(min_length=2, max_length=45, required=True)
    last_name = forms.CharField(min_length=2, max_length=45, required=True)

    class Meta():
        model = User
        fields =  'first_name', 'last_name', 'phone'

class BankAccountUpdateForm(forms.ModelForm):

    class Meta():
        model = User
        fields = 'bank_account_holder', 'bank_account_type', 'institution_number', 'branch_number', 'account_number'

class BuildingForm(forms.ModelForm):

    class Meta():
        model = Building
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.user = kwargs.pop('user',None)
        super(BuildingForm, self).__init__(*args, **kwargs)
        organization = self.user.organization
        self.fields['landlord'] = forms.ModelChoiceField(queryset=LandlordProfile.objects.filter(user__organization=organization))


class BookingCreateForm(forms.ModelForm):
    check_in = forms.CharField(widget=forms.TextInput(attrs={'type':'date'}))
    check_out = forms.CharField(widget=forms.TextInput(attrs={'type':'date'}))

    class Meta():
        model = Booking
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        
        self.user = kwargs.pop('user',None)
        # import pdb; pdb.set_trace()
        super(BookingCreateForm, self).__init__(*args, **kwargs)
        organization = self.user.organization
        self.fields['tenant'] = forms.ModelChoiceField(queryset=TenantProfile.objects.filter(user__organization=organization))
        self.fields['building'] = forms.ModelChoiceField(queryset=Building.objects.filter(landlord__user__organization=organization))
  


"""
Extends all-auth SignupForm to add 'user_type' field
and save. For regular signups.
"""
class CustomSignupForm(SignupForm):
    USER_TYPES = [
        (1,'tenant'),
        (2,'landlord'),
        (3,'merchant'),
        (4,'property manager'),

    ]
    user_type = forms.IntegerField(validators=[validate_user_type], widget=forms.Select(choices= USER_TYPES))

    def save(self, request):
        # import pdb; pdb.set_trace()        
        user = super(CustomSignupForm, self).save(request)
        data = self.cleaned_data
        user.user_type = data['user_type']    
        user.save()
        return user

class InviteeSignupForm(SignupForm):
    email=forms.EmailField(label='Email')

    def save(self, request):
        # import pdb; pdb.set_trace()
        user = super(InviteSignupForm, self).save(request)
        user.user_type = self.cleaned_data['user_type']
        user.save()
        return user










# class CreateNewPasswordForm(UserChangeForm):

#     USER_TYPE_CHOICES  = [
#         (1, 'tenant'),
#         (2, 'landlord'),
#         (3, 'merchant'),
#         (4, 'property manager'),
#     ]

#     user_type = forms.IntegerField(widget=forms.Select(choices= USER_TYPE_CHOICES))
#     password = forms.CharField(widget=forms.PasswordInput)

#     class Meta(UserChangeForm.Meta):
#         model = User
#         fields = ('user_type','password')


# class CreateUserForm(forms.Form):
#     USER_TYPES = [
#         (1,'tenant'),
#         (2,'landlord'),
#         (3,'merchant')
#     ]
#     first_name = forms.CharField(max_length=45)
#     last_name = forms.CharField(max_length=45)
#     email = forms.EmailField(label='Email', max_length=45)
#     user_type = forms.CharField(widget=forms.Select(choices=USER_TYPES))

# class NewUserForm(SignupForm):
#     USER_TYPES = [
#         (1,'Tenant'),
#         (2,'Owner'),
#         (4,'Merchant')
#     ]
#     first_name = forms.CharField(max_length=45)
#     last_name = forms.CharField(max_length=45)
#     user_type = forms.CharField(widget=forms.Select(choices=USER_TYPES))
#     password1 = forms.CharField(widget = forms.PasswordInput(render_value = True), initial='hello')
#     password2 = forms.CharField(widget = forms.PasswordInput(render_value = True))

    # class Meta(UserChangeForm.Meta):
    #     model = User
    #     fields = '__all__'
    # def __init__(self, *args, **kwargs):
    #    super(NewUserForm, self).__init__(*args, **kwargs)
    #    del self.fields['password1']
    #    del self.fields['password2']

    # def save(self, request):