
RENTAL_OPTIONS = [
    (1, 'Per entire unit'),
    (2, 'Per room'),
    (3, 'Both'),
]

UNIT_TYPE = [
    (1, 'Entire unit'),
    (2, 'Private room'),
]

UNIT_SIZE_OPTIONS = [
    (1, '1-bedroom'),
    (2, '2-bedroom'),
    (3, '3-bedroom'),
    (4, '4-bedroom'),
    (5, '5-bedroom'),
    (6, '6-bedroom'),
    (7, '7-bedroom'),
    (8, '8-bedroom'),
]