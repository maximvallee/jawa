from django.shortcuts import render, redirect
from accounts.models import *
# from accounts.forms import UserTypeSelectForm, UserInviteForm, UninvitedPostSignupForm, InviteePostSignupForm, InviteeSignupForm, OrganizationPostSignupForm, UserUpdateForm
from accounts.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from invitations.utils import get_invitation_model
from allauth.account.views import logout, SignupView
from django.contrib import messages #To display a message prompt
from jawa.decorators import post_signup_required, parent_only, children_only
from django.contrib.auth.decorators import user_passes_test
from itertools import chain
from operator import attrgetter
from django.http import HttpResponse
from django.utils.text import slugify

@login_required
@user_passes_test(children_only, redirect_field_name='/')
def post_signup_uninvited(request):
    # import pdb; pdb.set_trace()
    user = request.user
    if request.method == "POST":
        form = UninvitedPostSignupForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.organization = OrganizationProfile.objects.get(id=data['organization'])
            user.save()


            # organization = OrganizationProfile.objects.get(id=data['organization'])
            # if user.user_type == 1:
            #     user.tenant_profile.organization = organization
            #     user.tenant_profile.save()
            # elif user.user_type == 2:
            #     user.landlord_profile.organization = organization
            #     user.landlord_profile.save()
            # elif user.user_type == 3:
            #     user.merchant_profile.organizations.add(organization)
            #     user.merchant_profile.save() 
            
            
            
            return redirect('dashboard:dashboard')
    else:
        form = UninvitedPostSignupForm()

    return render(request, 'accounts/post_signup_form.html', {'form':form})

@login_required
@user_passes_test(children_only, redirect_field_name='/')
def post_signup_invitee(request):
    # import pdb; pdb.set_trace()
    user = request.user
    if request.method == "POST":
        form = InviteePostSignupForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.save()
            return redirect('dashboard:dashboard')
    else:
        form = InviteePostSignupForm()

    return render(request, 'accounts/post_signup_form.html', {'form':form})



@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def post_signup_organization(request):
    
    user = request.user
    if request.method == "POST":
        # import pdb; pdb.set_trace()
        form = OrganizationPostSignupForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            user.first_name = data['contact_first_name']
            user.last_name = data['contact_last_name']
            # user.organization = OrganizationProfile.objects.get(id=data['organization'])
            # user.save()

            # organization = OrganizationProfile.objects.get(user=user)
            # organization.name = data['organization_name']
            # organization.save()

            organization = OrganizationProfile.objects.create(name=data['organization_name'])
            user.organization = organization
            user.save()


            ###########

            # organization = user.organization
            # organization.name = data['organization_name']
            # organization.save()

            return redirect('dashboard:dashboard')
    else:
        form = OrganizationPostSignupForm()
    return render(request, 'accounts/post_signup_form.html', {'form': form})

@login_required
def select_user_type(request):
    """
    FOR SOCIAL ACCOUNTS ONLY
    Social user is prompted to choose a user type, which would otherwise
    have been selected either during sign up (standard email) or
    choose by the inviter.
    """ 
    # import pdb; pdb.set_trace()
    user = request.user
    if request.method == "POST":
        form = UserTypeSelectForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user.user_type = data['user_type']
            user.save()

            if user.user_type == 1:
                TenantProfile.objects.update_or_create(user=user)
            elif user.user_type == 2:
                LandlordProfile.objects.update_or_create(user=user)
            elif user.user_type == 3:
                MerchantProfile.objects.update_or_create(user=user)
            elif user.user_type == 4:
                # organization = OrganizationProfile.objects.create(name="noname")
                # user.organization = organization
                # user.save()
                ManagerProfile.objects.update_or_create(user=user)

            return redirect('dashboard:dashboard')
    else:
        form = UserTypeSelectForm()
    return render(request, 'accounts/select_user_type_form.html', {'form': form})

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def invite_user(request):
    # import pdb; pdb.set_trace()
    if request.method == "POST":
        form = UserInviteForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Invitation = get_invitation_model()
            invite = Invitation.create(data['email'], inviter=request.user, user_type=data['user_type'])
            invite.send_invitation(request)
            return redirect('dashboard:dashboard')
    else:
        form = UserInviteForm()
    return render(request, 'accounts/invite_form.html', {'form': form})

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def create_building(request):
    
    if request.method == "POST":
        # import pdb; pdb.set_trace()
        form = BuildingForm(data=request.POST, user=request.user)
        if form.is_valid():
            data = form.cleaned_data
            obj = Building.objects.create(
                landlord=data['landlord'],
                address = data['address'],
                size = data['size'],
                bath = data['bath'],
                rental_type = data['rental_type'],
            )
            obj.save()

            return redirect('dashboard:dashboard')
    else:
        form = BuildingForm(user=request.user)
    return render(request, 'accounts/building_form.html', {'form': form})

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def update_building(request, pk):
    building = Building.objects.get(id=pk)

    if request.method == "POST":
        form = BuildingForm(data=request.POST, user=request.user)
        if form.is_valid():
            # import pdb; pdb.set_trace()
            data = form.cleaned_data
            building.landlord = data['landlord']
            building.address = data['address']
            building.unit = data['unit']
            building.save()
            return redirect('accounts:building_list')
    else:
        form = BuildingForm(instance=building, user=request.user)

    return render(request, 'accounts/building_form.html', {'form': form})

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def delete_building(request, pk):
    # import pdb; pdb.set_trace()
    building = Building.objects.get(id=pk)
    context = {"building":building}

    if request.method == 'POST':
        building.delete()
        return redirect('accounts:building_list')

    return render(request, "accounts/building_confirm_delete.html", context)


@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def create_booking(request):
    
    if request.method == "POST":
        # import pdb; pdb.set_trace()
        # form = BookingCreateForm(data=request.POST, user=request.user)
        form = BookingCreateForm(data=request.POST, user=request.user)
        if form.is_valid():
            data = form.cleaned_data
            booking = Booking.objects.create(
                status=data['status'],
                tenant = data['tenant'],
                building = data['building'],
                check_in = data['check_in'],
                check_out = data['check_out'],
                rent = data['rent'],
            )
            booking.save()

            return redirect('dashboard:dashboard')
    else:
        # form = BookingCreateForm(request.user)
        form = BookingCreateForm(user=request.user)
    return render(request, 'accounts/booking_form.html', {'form': form})




@login_required
# @user_passes_test(parent_only, redirect_field_name='/')
def notifications(request):

    notifications = ''
    user = request.user
    # import pdb; pdb.set_trace()
    if user.user_type == 4:
        organization = user.organization
        bookings = Booking.objects.filter(building__landlord__user__organization=organization, status=0)

        Invitation = get_invitation_model()
        invitations = Invitation.objects.filter(inviter=user, accepted=False)

        notifications = sorted(chain(bookings, invitations), key=attrgetter('created'), reverse=True)

    elif user.user_type == 1:
        tenant = user.tenant_profile
        notifications = Booking.objects.filter(tenant=tenant, status=0).order_by('-created')

    context = { 'notifications': notifications}
    # print(result_list)
    return render(request, 'accounts/notifications.html', context)
    # return HttpResponse(result_list)



@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def list_bookings(request):
    # import pdb; pdb.set_trace()
    organization = request.user.organization
    confirmed_bookings = Booking.objects.filter(building__landlord__user__organization=organization).exclude(status=0)

    context = {
        'bookings': confirmed_bookings,
    }
    return render(request, "accounts/booking_list.html", context)

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def list_buildings(request):
    # import pdb; pdb.set_trace()
    organization = request.user.organization
    buildings = Building.objects.filter(landlord__user__organization=organization)

    context = {
        'buildings': buildings,
    }
    return render(request, "accounts/building_list.html", context)


@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def resend_invite(request, pk):
    # import pdb; pdb.set_trace()
    Invitation = get_invitation_model()
    instance = Invitation.objects.get(id=pk)
    response = instance.send_invitation(request)
    message = 'Invitation resent.'

    return redirect('dashboard:dashboard')

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def list_profiles(request):
    # import pdb; pdb.set_trace()
    organization = request.user.organization

    if 'tenants' in request.path:
        user_profiles = TenantProfile.objects.filter(user__organization=organization)
        user_type = 1
    elif 'landlords' in request.path:
        user_profiles = LandlordProfile.objects.filter(user__organization=organization)
        user_type = 2
    elif 'merchants' in request.path:
        user_profiles = MerchantProfile.objects.filter(user__organization=organization)
        user_type = 3

    context = {
        'user_profiles': user_profiles,
        'user_type': user_type
    }
    # import pdb; pdb.set_trace()
    
    return render(request, "accounts/profile_list.html", context)

@login_required
@user_passes_test(parent_only, redirect_field_name='/')
def update_user(request, username):
    obj = User.objects.get(username=username)
    user_type = obj.user_type

    if user_type == 1:
        redirect_url = 'accounts:tenant_list'
    elif user_type == 2:
        redirect_url = 'accounts:landlord_list'
    elif user_type == 3:
        redirect_url = 'accounts:merchant_list'

    # import pdb; pdb.set_trace()
    if request.method == "POST":
        form = UserUpdateForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            obj.first_name = data['first_name']
            obj.last_name = data['last_name']
            obj.save()
            return redirect(redirect_url)
    else:
        form = UserUpdateForm(instance=obj)

    return render(request, 'accounts/update_user_form.html', {'form': form, 'redirect_url': redirect_url, 'user_type': user_type})


@login_required
def update_profile(request):
    user = request.user
    if request.method == "POST":
        form = ProfileUpdateForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.phone = data['phone']
            user.save()
            return redirect('accounts:settings')
    else:
        form = ProfileUpdateForm(instance=request.user)

    return render(request, 'accounts/profile_details_form.html', {'form': form})

@login_required
def update_bank_account(request):
    user = request.user
    if request.method == "POST":
        form = BankAccountUpdateForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user.bank_account_holder = data['bank_account_holder']
            user.bank_account_type = data['bank_account_type']
            user.institution_number = data['institution_number']
            user.branch_number = data['branch_number']
            user.account_number = data['account_number']
            user.save()
            return redirect('accounts:settings')
    else:
        form = BankAccountUpdateForm(instance=request.user)

    return render(request, 'accounts/bank_account_form.html', {'form': form})


"""
Extends all-auth's SignUpView to customize signup form
so that it does not ask for 'user_type', but rather pulls
it from the invitation by overriding the form's save()
"""

class InviteeSignupView(SignupView):
    template_name = 'accounts/invitee_signup_form.html'
    form_class = InviteeSignupForm
    view_name = 'invitee_signup'

    # I don't use them, but you could override them
    # (N.B: the following values are the default)
    # success_url = None
    # redirect_field_name = 'next'

# Create the view (we will reference to it in the url patterns)
invitee_signup = InviteeSignupView.as_view()

@login_required
def settings(request):
    return render(request, "accounts/settings.html")

@login_required
def account_confirm_deactivate(request):
    # import pdb; pdb.set_trace()
    user = request.user

    if request.method == 'POST':
        user.is_active = False
        user.save()
        logout(request)
        return redirect('/')

    return render(request, "accounts/account_confirm_deactivate.html")












# Create your views here.
# def select_user_type(request, pk):
#     # import pdb; pdb.set_trace()
#     obj = User.objects.get(id=pk)

#     ##Only allow to update one user's own profile
#     if request.user.id == pk:
#         if request.method== "POST":
#             form = SelectUserTypeForm(request.POST)
#             if form.is_valid():
#                 data = form.cleaned_data
#                 obj.user_type = data['user_type']
#                 obj.save()
#             return redirect('dashboard:home')
#         else:
#             ##Only allow to render and update user type if it is 'null' else redirect home
#             # if obj.user_type == 0: 
#             #     form = SelectUserTypeForm()
#             # import pdb; pdb.set_trace()
#             # obj.set_unusable_password()
#             if obj.user_type == 0 and not obj.has_usable_password(): 
#                 # form = SelectUserTypeForm()
#                 return redirect('user:password_create', pk=pk)
#             else:
#                 return redirect('dashboard:home')
#     else:
#         return redirect('dashboard:home')

#     return render(request, 'accounts/user_type_form.html', {'form': form})


# def create_password(request, pk):
    
#     obj = User.objects.get(id=pk)
#     if request.method== "POST":
        
#         form = CreateNewPasswordForm(request.POST)
#         # import pdb; pdb.set_trace()
#         # data = form.cleaned_data
#         obj.user_type = request.POST['user_type']
#         obj.save()
#         obj.set_password(request.POST['password'])
#         obj.save()

#         # if form.is_valid():
#         #     data = form.cleaned_data
#         #     obj.user_type = data['user_type']
#         #     obj.save()
#             # obj.set_password(data['password'])
#             # obj.save()
#         return redirect('dashboard:home')

#     else:
#         form = CreateNewPasswordForm()
    
#     return render(request, 'accounts/create_password_form.html', {'form': form})
















# @login_required
# def create_user(request):
#     if request.method == "POST":
#         form = CreateUserForm(request.POST)
#         if form.is_valid():
#             data = form.cleaned_data
#             import pdb; pdb.set_trace()
#             new_user = User(
#                 first_name=data['first_name'],
#                 last_name=data['last_name'],
#                 email=data['email'],
#                 username=data['email'].split("@")[0],
#                 user_type=data['user_type'],
#                 created_by=request.user
#             )
#             new_user.save()
#             new_user.set_password(generate_password())
#             new_user.save()

            # import pdb; pdb.set_trace()
            # reset_password_form = PasswordResetForm(data={'email': new_user.email})
            # if reset_password_form.is_valid():
            #     reset_password_form.save(request=request) # The save method will send the email

            # form = PasswordResetForm({'email': new_user.email})

            # if form.is_valid():
            #     # request = HttpRequest()
            #     request.META['SERVER_NAME'] = '127.0.0.1:8000'
            #     request.META['SERVER_PORT'] = '80'
            #     form.save(
            #         request= request,
            #         # use_https=True,
            #         from_email="username@gmail.com", 
            #         email_template_name='registration/password_reset_email.html')

            
            # print('save form')
    #     return redirect('dashboard:home')
    # else:
    #     form = CreateUserForm()
    #     print('load form')
    # return render(request, 'accounts/create_user_form.html',{'form': form})



# class NewUserSignupView(SignupView):
# # class NewUserSignupView(LoginRequiredMixin, SignupView):
#     # The referenced HTML content can be copied from the signup.html
#     # in the django-allauth template folder
#     template_name = 'accounts/new_user_signup_form.html'
#     # the previously created form class
#     form_class = NewUserForm

#     # the view is created just a few lines below
#     # N.B: use the same name or it will blow up
#     view_name = 'new_user_signup'

#     def form_valid(self, form):
#         # By assigning the User to a property on the view, we allow subclasses
#         # of SignupView to access the newly created User instance
#         import pdb; pdb.set_trace()
#         self.user = form.save(self.request)
#         try:
#             return complete_signup(
#                 self.request,
#                 self.user,
#                 app_settings.EMAIL_VERIFICATION,
#                 self.get_success_url(),
#             )
#         except ImmediateHttpResponse as e:
#             return e.response
#         return super(NewUserSignupView, self).form_valid(form)

    # I don't use them, but you could override them
    # (N.B: the following values are the default)
    # success_url = None
    # redirect_field_name = 'next'
    # def get_success_url(self):
    #     # Explicitly passed ?next= URL takes precedence
    #     ret = (' '
            
    #     )
    #     return ret


# Create the view (we will reference to it in the url patterns)
# new_user_signup = NewUserSignupView.as_view()













# @login_required
# def delete_account(request):

#     return render(request, "accounts/account_confirm_delete.html")

# from django.shortcuts import get_object_or_404


